const express = require('express')
const path = require('path');
const app = express()

app.use(express.static('styles'))
app.use(express.static('js'))


app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'))
})

app.get('/flights', (req, res) => {
    res.sendFile(path.join(__dirname + '/flights.html'))
})

app.get('/hotels', (req, res) => {
    res.sendFile(path.join(__dirname + '/hotels.html'))
})

app.listen(3000, () => {
    console.log('Server started on port 3000')
})

module.exports = { app }