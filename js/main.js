const main = {
    onDocumentReady: function () {

        let timeout
        let delay = 1000

        let searchBox = document.getElementById('flightSearchBox')
        if (searchBox !== null) {
            searchBox.addEventListener("keyup", searchFlights)
        }

        //Function to search flights
        function searchFlights(e) {

            //Timeout for user keypress
            if (timeout) {
                document.getElementById('flightBox').classList.add('is-loading')
                clearTimeout(timeout)
            }
            //Search triggered
            document.getElementById('flights').innerHTML = ""
            timeout = setTimeout(function () {
                document.getElementById('flightBox').classList.remove('is-loading')
                let destinationSearch = document.getElementById('flightSearchBox').value
                //Setup airport code search
                let citysearch = new Request('https://test.api.amadeus.com/v1/reference-data/locations?subType=AIRPORT,CITY&keyword=' + destinationSearch + '&page[limit]=5', {
                    method: "GET",
                    headers: new Headers({
                        "Authorization": "Bearer GMbBV8NC46daAFMsfeQA7Jwz6YNF"
                    })
                })
                //Fetch the airport code
                fetch(citysearch).then(response => response.json()).then(data => {
                    console.log('Airport iataCode:', data.data[0].iataCode)

                    let airportCode = data.data[0].iataCode
                    //Setup destinations search
                    let destinationsrequest = new Request('https://test.api.amadeus.com/v1/shopping/flight-destinations?origin=' + airportCode, {
                        method: "GET",
                        headers: new Headers({
                            "Authorization": "Bearer GMbBV8NC46daAFMsfeQA7Jwz6YNF"
                        })
                    })
                    //Fetch the flights
                    fetch(destinationsrequest).then(response => response.json())
                        .then(data => {
                            let array = data.data
                            for (let i = 0; i < array.length; i++) {
                                //Instantiate an object which will contain all the props needed
                                let flights = {
                                    depart: array[i].origin,
                                    destinations: array[i].destination,
                                    departures: array[i].departureDate,
                                    returns: array[i].returnDate,
                                    prices: array[i].price.total
                                }




                                //Create the cards with the fetched info
                                document.getElementById('flights').innerHTML +=
                                    '<div class="columns is-vcentered is-centered">' +
                                    '<div class="column is-two-thirds">' +
                                    '<div class="card">' +
                                    '<header class="card-header">' +
                                    '<p class="card-header-title is-centered">Flight</p>' +
                                    '</header >' +
                                    '<div class="card-content">' +
                                    '<div class="columns is-vcentered is-centered">' +
                                    '<div class="column is-one-third is-5">' +
                                    '<div class="content">' + '<strong>Departs from: </strong>' + flights.depart +
                                    '<br>' +
                                    '<strong>Goes to: </strong>' + flights.destinations +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="column">' +
                                    '<div class="content">' +
                                    '<p><strong>Depart day: </strong>' + flights.departures + '</p>' +
                                    '</div>' +
                                    '<p><strong>Return day: </strong>' + flights.returns + '</p>' +
                                    '</div>' +
                                    '<div class="column">' +
                                    '<div class="container">' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="column is-one-third is-1">' +
                                    '<div class="container">' +
                                    '<br>' +
                                    '<div class="content">' +
                                    '<p><strong>Price: </strong>' + flights.prices + '€</p>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<footer class="card-footer">' +
                                    '<a href="#" class="card-footer-item"><i class="fas fa-shopping-cart"></i><span></span> Book now</a>' +
                                    '<a href="#" class="card-footer-item"><i class="fas fa-info-circle"></i><span></span> View Details</a>' +
                                    '</footer>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<br>'
                            }
                        })
                })
            }, delay)
        }

        hotelSearchButton = document.getElementById('hotelButton')
        if (hotelSearchButton !== null) {
            hotelSearchButton.addEventListener('click', searchHotels)
        }

        //Function for searching hotels
        function searchHotels() {
            document.getElementById('hotels').innerHTML = ""
            //Get the requested value
            console.log('Left the first function, now inside searchHotels!')
            let hotelCity = document.getElementById('hotelSearch').value
            let distanceValue = document.getElementById('distanceInput').value
            let peopleValue = document.getElementById('adultSelector').value
            let roomValue = document.getElementById('roomSelector').value
            //Setup the request
            let cityCode = new Request('https://test.api.amadeus.com/v1/reference-data/locations?subType=AIRPORT,CITY&keyword=' + hotelCity, {
                method: "GET",
                headers: new Headers({
                    "Authorization": "Bearer GMbBV8NC46daAFMsfeQA7Jwz6YNF"
                })
            })
            //Make the request to get the cityCode and store the value in a variable
            fetch(cityCode).then(response => response.json()).then(data => {
                console.log('City iataCode:', data.data[0].iataCode)

                let cityCode = data.data[0].iataCode
                //Setup the request for hotels search
                let hotelsrequest = new Request('https://test.api.amadeus.com/v2/shopping/hotel-offers?cityCode=' + cityCode + '&roomQuantity=' + roomValue + '&adults=' + peopleValue + '&radius=' + distanceValue + '&radiusUnit=KM&paymentPolicy=NONE&includeClosed=false&bestRateOnly=true&view=FULL&sort=PRICE', {
                    method: "GET",
                    headers: new Headers({
                        "Authorization": "Bearer GMbBV8NC46daAFMsfeQA7Jwz6YNF"
                    })
                })
                //Make the call to the API to get Hotel offers
                fetch(hotelsrequest).then(response => response.json()).then(data => {
                    let array = data.data

                    for (i = 0; i < array.length; i++) {
                        let results = Object.values(array[i])
                        let hotelArray = results[1]
                        let hotelPriceArr = results[3]
                        let hotelPlus = hotelPriceArr[0]
                        let hotelInfo = {
                            name: hotelArray.name,
                            description: hotelArray.description.text,
                            rating: hotelArray.rating,
                            contactphone: hotelArray.contact.phone,
                        }

                        let hotelPrices = {
                            currency: hotelPlus.price.currency,
                            price: hotelPlus.price.total,
                        }

                        document.getElementById('hotels').innerHTML +=
                            '<div class="columns is-multiline is-vcentered is-centered">' +
                            '<div class="column is-two-thirds">' +
                            '<p class="bd-notification is-info"></p>' +
                            '<div class="columns is-mobile">' +
                            '<div class="column">' +
                            '<div class="box">' +
                            '<article class="media">' +
                            '<div class="media-left">' +
                            '<figure class="image is-64x64">' +
                            '<img src="https://hotelvillabelvedere.it/thumbsx.aspx?Id=1&cosa=Gallery&Lato=256&Formato=X&src=Hotel_Villa_Belvedere_Cefalu_Piscina-05.jpg" alt="Image">' +
                            '</figure>' +
                            '</div>' +
                            '<div class="media-content">' +
                            '<div class="content">' +
                            '<p>' +
                            '<strong>' + hotelInfo.name + '</strong>' +
                            '<br>' +
                            '<p class="is-small">Stars ⭐: ' +
                            + hotelInfo.rating +
                            '</p>' +
                            '<p>' +
                            hotelInfo.description +
                            '</p>' +
                            '<p>Price: ' +
                            + hotelPrices.price + hotelPrices.currency +
                            '</p>' +
                            '<p>📞 Call us at: ' +
                            hotelInfo.contactphone +
                            '</p>' +
                            '</div>' +
                            '<nav class="level is-mobile">' +
                            '<div class="level-left">' +
                            '<a class="level-item" aria-label="reply">' +
                            '<span class="icon is-small">' +
                            '<i class="fas fa-shopping-cart" aria-hidden="true"></i>' +
                            '</span>' +
                            '</a>' +
                            '<a class="level-item" aria-label="retweet">' +
                            '<span class="icon is-small">' +
                            '<i class="fas fa-info" aria-hidden="true"></i>' +
                            '</span>' +
                            '</a>' +
                            '</span>' +
                            '</a>' +
                            '</div >' +
                            '</nav >' +
                            '</div >' +
                            '</article >' +
                            '</div >' +
                            '</div>' +
                            '</div>' +
                            '</div>'

                        console.log('Hotel name: ', hotelInfo.name)
                        console.log('Hotel description: ', hotelInfo.description)
                        console.log('Hotel rating: ', hotelInfo.rating)
                        console.log('Hotel phone contact: ', hotelInfo.contactphone)
                        console.log('Hotel fax contact: ', hotelInfo.contactfax)
                        console.log('Guests: ', hotelPrices.guests)
                        console.log('Currency: ', hotelPrices.currency)
                        console.log('Price: ', hotelPrices.price)
                    }
                })
            })
        }

    }
}
main.onDocumentReady()